const yaml = require('js-yaml')
const fs = require('fs')
const path = require('path')

let filePath = process.env.QUOTE 

let content = yaml.safeLoad(fs.readFileSync(`${filePath}.yml`, 'utf8'))

let prices = content.items.map(item => {
  let calcItem = Object.assign({
    total: item.price * item.quantity,
    totalTax: (item.price * item.quantity) * (1 + item.taxRate / 100),
    tax: (item.price * item.quantity) * item.taxRate / 100
  }, item)
  return calcItem
})

let quote_preview = [
  `- Devis ID: ${content.id}`,
  `- **Client: ${content.client}**`,
  `- Description: ${content.description}`,
  `- Date: ${content.date}`,
  `---`  
]

let quote = [
  `- Devis ID: ${content.id}`,
  `- **Client: ${content.client}**`,
  `- Description: *${content.description}*`,
  `- Date: ${content.date}`,
  `<hr>`,
  "",
  "| Description | Qty | Price | Qty x Price | Tax Rate | Tax | Total + Tax |",
  "| ----------- | --- | ----- | ----------- | -------- | --- | ----------- |"
]

var totalQuotePrice = 0
var totalQuoteTaxPrice = 0
var totalTax = 0

prices.forEach(item => {

  let unitPrice = new Intl.NumberFormat('eu-EU',{style:'currency', currency:'EUR'}).format(item.price)
  let totalPrice = new Intl.NumberFormat('eu-EU',{style:'currency', currency:'EUR'}).format(item.total)
  let totalTaxPrice = new Intl.NumberFormat('eu-EU',{style:'currency', currency:'EUR'}).format(item.totalTax)
  let taxPrice =  new Intl.NumberFormat('eu-EU',{style:'currency', currency:'EUR'}).format(item.tax)

  totalQuotePrice += item.total
  totalQuoteTaxPrice += item.totalTax
  totalTax += item.tax

  quote_preview.push(`- ${item.description}: ${item.quantity} x ${unitPrice} = ${totalPrice} `)

  quote.push(
    `| ${item.description} | ${item.quantity} | ${unitPrice} | ${totalPrice} | ${item.taxRate}% | ${taxPrice} | ${totalTaxPrice} |`
  )
})

quote_preview.push(
  `---`,
  `- **Total HT**: ${new Intl.NumberFormat('eu-EU',{style:'currency', currency:'EUR'}).format(totalQuotePrice)} `
)


quote.push(
  `|  |  |  |  |  | **Total HT** | ${new Intl.NumberFormat('eu-EU',{style:'currency', currency:'EUR'}).format(totalQuotePrice)} |`,
  `|  |  |  |  |  | **Total Tax** | ${new Intl.NumberFormat('eu-EU',{style:'currency', currency:'EUR'}).format(totalTax)} |`,
  `|  |  |  |  |  | **Total TTC** | ${new Intl.NumberFormat('eu-EU',{style:'currency', currency:'EUR'}).format(totalQuoteTaxPrice)} |`,
  ``
)

fs.writeFileSync(`${filePath}-preview.md`, quote_preview.join("\n"));


fs.writeFileSync(`${filePath}.md`, quote.join("\n"));
console.log(`👋 markdown quote generated: ${filePath}.md`)
